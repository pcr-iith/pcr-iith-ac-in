---
layout: post
title: IIT Hyderabad Researchers use corn husk to produce carbon electrode for high-voltage supercapacitors
event_date: 29-07-2020
categories: pressrelease
link: Press Release - IIT Hyderabad Researchers use corn husk to produce carbon electrode for high-voltage supercapacitors-29-07-2020.pdf
---