---
layout: post
title: IIT Hyderabad Bio Researchers reveals neurotoxic nature of Triclosan an antimicrobial chemical used in soaps and dental care products.
event_date: 15-12-2020
categories: pressrelease
link: Press Note -IIT Hyderabad Bio Researchers reveals neurotoxic nature of Triclosan an antimicrobial chemical used in soaps and dental care products-15-12-2020.pdf
---