---
layout: post
issue: Issue 1, January 2020 
title: An Abstract of 2019
event_date: 01-01-2020
categories: newsletter
img: issue1.jpeg
link: Issue-1.pdf
flipbook: https://online.fliphtml5.com/sxjjb/hyhw/
issueNumber: 1
---